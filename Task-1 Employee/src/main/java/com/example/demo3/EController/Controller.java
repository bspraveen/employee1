package com.example.demo3.EController;

	import java.util.List;

	import org.springframework.beans.factory.annotation.Autowired;
	import org.springframework.web.bind.annotation.DeleteMapping;
	import org.springframework.web.bind.annotation.GetMapping;
	import org.springframework.web.bind.annotation.PathVariable;
	import org.springframework.web.bind.annotation.PostMapping;
	import org.springframework.web.bind.annotation.PutMapping;
	import org.springframework.web.bind.annotation.RequestBody;
	import org.springframework.web.bind.annotation.RestController;
	import com.example.demo3.Emodel.Employee;
	import com.example.demo3.Eservice.EService;
	
	
	@RestController
	public class Controller {

	@Autowired
	EService employeeService;
	   

	@GetMapping("/emp")
	private List<Employee> getAllEmployee()
	{
	return employeeService.getAllEmployee();
	}

	@GetMapping("/emp/{ID}")
	private Employee getEmployee(@PathVariable("ID") int ID)
	{
	return employeeService.getEmployeeById(ID);
	}

	@DeleteMapping("/emp/{ID}")
	private void delete(@PathVariable("ID") int ID)
	{
	employeeService.delete(ID);
	}

	@PostMapping("/emps")
	private int saveEmployee(@RequestBody Employee employee)
	{
	employeeService.saveOrUpdate(employee);
	return employee.getEmployeeId();
	}

	@PutMapping("/emps")
	private Employee update(@RequestBody Employee employee)
	{
	employeeService.saveOrUpdate(employee);
	return employee;
	}
	}




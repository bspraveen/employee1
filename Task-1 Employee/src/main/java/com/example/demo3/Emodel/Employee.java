package com.example.demo3.Emodel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class Employee {
     
@Id
@Column
private int employeeId;
@Column
private String employeeName;
@Column
private int salary;
public int getEmployeeId() {
return employeeId;
}
public void setEmpid(int employeeId) {
this.employeeId = employeeId;
}
public String getEmployeeName() {
return employeeName;
}
public void setEmpname(String employeeName) {
this.employeeName = employeeName;
}
public int getSalary() {
return salary;
}
public void setSalary(int salary) {
this.salary = salary;
}
}


package com.example.demo3.Eservice;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo3.Emodel.Employee;
import com.example.demo3.Erepository.*;


@Service
public class EService {


@Autowired
EmployeeRepository employeeRepository;

public List<Employee> getAllEmployee()
{
List<Employee> employee = new ArrayList<Employee>();
employeeRepository.findAll().forEach(employee1 -> employee.add(employee1));
return employee;
}

public Employee getEmployeeById(int ID)
{
return employeeRepository.findById(ID).get();
}

public void saveOrUpdate(Employee employee)
{
employeeRepository.save(employee);
}

public void delete(int ID)
{
employeeRepository.deleteById(ID);
}

public void update(Employee employee, int ID)
{
    employeeRepository.save(employee);
}
}
